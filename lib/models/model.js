'use strict';

const { Model } = require('schwifty');
const schema = require('../schemas/schema');
const encrypt = require('@arkandros/iut-encrypt');

module.exports = class UserModel extends Model {
    static tableName(){
        return 'table_users';
    }

    static get joiSchema() {
        return schema;
    }

    static encryptPassword(password){
        return encrypt.encryptInSHA1(password);
    }
}
