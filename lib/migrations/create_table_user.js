const encrypt = require('@joffrey1323/iut-encrypt');
const UserModel = require('../models/model');

exports.up = function(knex,Promise) {
    return Promise.all([
        knex.schema.createTable(UserModel.tableName(),(table) => {
            table.increments('id').primary();
            table.string('login').unique();
            table.string('password');
            table.string('email').unique();
            table.string('firstname');
            table.string('lastname');
            table.string('company');
            table.string('function');
        }).then(function () {
            return knex(UserModel.tableName()).insert({
                login: "bethoule06",
                password: encrypt.encryptInSHA1("mdp"),
                email : "yohann.bethoule@etu.unilim.fr",
                firstname : "Yohann",
                lastname : "Bethoule",
                company: "Iconosquare",
                function : "Developer"
            })
        }),
    ]);
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists(UserModel.tableName())
    ]);
};
