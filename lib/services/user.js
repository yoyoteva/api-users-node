'use strict';

const { Service } = require('schmervice');
const { Boom } = require('boom');

module.exports = class UserService extends Service{
    async insertUser(user){
        const {UserModel} = this.server.models();
        user.password = UserModel.encryptPassword(user.password);
        return await UserModel.query().insert(user)
    }

    async getAllUsers(){
        const {UserModel} = this.server.models();
        return await UserModel.query();
    }

    async getUserById(id){
        const {UserModel} = this.server.models();
        return await UserModel.query()
            .where('id', id);
    }

    async update(id_user, payload){
        try{
            const {UserModel} = this.server.models();

            if ('password' in payload){
                payload.password = UserModel.encryptPassword(payload.password)
            }
            return await UserModel.query()
                .where('id',id_user)
                .patch(payload);
        }catch(e){
            return await Boom.conflict(e, {statusCode: 404});
        }
    }

    async delete(id_user){
        try{
            const {UserModel} = this.server.models();
            return await UserModel.query()
                .where('id',id_user)
                .del();
        }catch(e){
            Boom.conflict(e, {statusCode: 404});
        }
    }

    async generate(array_users){
        const {UserModel} = this.server.models();

        for (let i=0; i<array_users.length; i++){
            array_users[i].password = UserModel.encryptPassword(array_users[i].password);
        }

        return await UserModel.query().insert(array_users);
    }

    async auth(infos){
        const {UserModel} = this.server.models();

        var login = infos.login;
        var password = UserModel.encryptPassword(infos.password);

        return await UserModel.query()
            .where({
                login: login,
                password: password
            });
    }
}