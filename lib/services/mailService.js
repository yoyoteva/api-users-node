'use strict';

const { Service } = require('schmervice');
const nodeMailer = require('nodemailer')

const transporter = nodeMailer.createTransport('smtps://yoyoteva@gmail.com:mdp@smtp.gmail.com');

module.exports = class MailService extends Service {
    async sendMailLogin(user){
        var mailOptions = {
            from: 'yoyoteva@gmail.com',
            to: user.email,
            subject: 'Login and password',
            html: '<h1>Your new login and password: </h1>' +
                '<br> ' +
                '<p>Login:</p>' + user.login +
                '<p>Password:</p>' + user.password
        };

        await transporter.sendMail(mailOptions,await function (error, info) {
            if (error) {
                console.log(error);
            }

            console.log('Message sent :' + info.response);
        })
    }

    async sendMailChangeLoginOrdPassword(user,changedInfos){

        console.log(changedInfos);
        var mailOptions = {
            from: 'yoyoteva@gmail.com', // sender address
            to: user[0].email, // list of receivers
            subject: 'Login and password have changed', // Subject line
            html: '<h1>Your login or your password have changed</h1>'
        };

        await transporter.sendMail(mailOptions,await function (error, info) {
            if (error) {
                console.log(error);
            }

            console.log('Message sent :' + info.response);
        })
    }
}
