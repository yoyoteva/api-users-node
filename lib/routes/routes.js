// Add the route
const Joi = require('joi')
const faker = require('faker')
const schema = require('../schemas/schema')
const Boom = require('boom')

module.exports = [
    {
        method: 'get',
        path: '/users',
        config: {
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            }
        },
        handler: async (request, h) => {
            const {userService} = request.services();
            return await userService.getAllUsers(request)
        }
    },
    {
        method: 'post',
        path: '/users',
        options: {
            description: 'Add new user',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            validate : {
                payload : {
                    schema
                }
            }
        },
        async handler (request, h){
            const { userService } = await request.services();
            const { mailService } = await request.services();

            await mailService.sendMailLogin(user);
            await userService.insertUser(user);

            return h.response('User inserted with success !').code(201);
        }
    },
    {
        method: 'post',
        path: '/users/generate',
        options: {
            description: 'Add new user',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            }
        },
        async handler(request, h){
            const {userService} = await request.services();

            var array_users = [];

            for (let i = 0; i < 100; i++) {
                var newUser = {
                    login : faker.internet.userName(),
                    password : faker.internet.password(),
                    email : faker.internet.email(),
                    firstname : faker.name.firstName(),
                    lastname : faker.name.lastName(),
                    company : faker.company.companyName(),
                    function : faker.name.jobTitle()
                };
                array_users.push(newUser);
            }
            return await userService.generate(array_users);
        },
    },
    {
        method: 'get',
        path: '/user/{id}',
        config: {
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form',
                    response: {
                        emptyStatusCode: 404
                    },
                }
            },
            validate: {
                params: {
                    id: Joi.number()
                }
            },
            async handler(request, h){
                const {userService} = request.services();

                var res = await userService.getUserById(request.params.id);

                if(res.length === 0){
                    throw Boom.notFound('User not found');
                }

                return res;
            }
        }
    },
    {
        method: 'put',
        path: '/user/{id}',
        config: {
            tags: ['api'],
            description: 'Update specific user data',
            plugins: {
                'hapi-swagger': {
                    userType: 'form',
                    responses: {
                        '404': {
                            'description': 'BadRequest'
                        }
                    },
                }
            },
            validate: {
                params: {
                    id: Joi.number()
                },
                payload: {
                    login: Joi.string(),
                    password: Joi.string().alphanum().min(8),
                    email: Joi.string().email(),
                    firstname: Joi.string(),
                    lastname: Joi.string(),
                    company: Joi.string(),
                    function: Joi.string(),
                }
            },
            async handler(request, h){
                const {userService} = request.services();
                const {mailService} = request.services();

                var res = await userService.update(request.params.id,request.payload);
                var user = await userService.getUserById(request.params.id);

                if (request.payload.login!==undefined || request.payload.password!==undefined ){
                    await mailService.sendMailChangeLoginOrdPassword(user,request.payload);
                }

                if(res===0){
                    throw Boom.notFound('User not found');
                }

                return res;
            }
        }
    },
    {
        method: 'delete',
        path: '/user/{id}',
        options: {
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            description: 'Delete user',
            validate: {
                params: {
                    id: Joi.number()
                }
            },
            async handler(request, h){
                const {userService} = request.services();

                var res = await userService.delete(request.params.id);

                if(res===0){
                    throw Boom.notFound('User not found');
                }

                return h.response("User deleted").code(204);
            },
        }
    },
    {
        method: 'post',
        path: '/auth/{login}/{password}',
        options: {
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            description: 'Authentification',
            validate: {
                params: {
                    login: Joi.string(),
                    password: Joi.string()
                }
            }
        },
        async handler(request, h){
            const {userService} = request.services();

            var res = await userService.auth(request.params);

            if (res.length === 0) {
                return h.response('ko').code(404);
            }

            return h.response('OK').code(204);
        }
    },
];
