'use strict';
const Dotenv = require('dotenv');
const Confidence = require('confidence');
const Toys = require('toys');
const schwifty = require('schwifty');
const dotenv = require('dotenv').config();

// Pull .env into process.env
Dotenv.config({ path: `${__dirname}/.env` });

// Glue manifest as a confidence store
module.exports = new Confidence.Store({
    server: {
        host: 'localhost',
        port: {
            $env: 'PORT',
            $coerce: 'number',
            $default: 3000
        },
        debug: {
            $filter: { $env: 'NODE_ENV' },
            $default: {
                log: ['error'],
                request: ['error']
            },
            production: {
                request: ['implementation']
            }
        }
    },
    register: {
        plugins: [
            {
                plugin: '../lib',
                options: {}
            },
            {
                plugin: './plugins/swagger'
            },
            {
                plugin: 'schwifty',
                options: {
                    $filter: 'NODE_ENV',
                    $default: {},
                    $base: {
                        migrateOnStart: true,
                        knex: {
                            client: 'mysql',
                            connection: {
                                host     : process.env.MYSQL_HOST || 'localhost',
                                port     : process.env.MYSQL_PORT || 3306 ,
                                user     : process.env.MYSQL_USER || 'root',
                                password : process.env.MYSQL_PASSWORD || '',
                                database : process.env.MYSQL_DATABASE || 'base_node'
                            }
                        }
                    },
                    production: {
                        migrateOnStart: false
                    }
                }
            },
            {
                plugin: {
                    $filter: { $env: 'NODE_ENV' },
                    $default: 'hpal-debug',
                    production: Toys.noop
                }
            }
        ]
    }
});
